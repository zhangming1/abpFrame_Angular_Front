import { Component, HostListener, Injector } from '@angular/core';
import * as screenfull from 'screenfull';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'header-fullscreen',
  template: `
  <div (click)="_click()"  class="alain-default__nav-item">
    <i nz-icon [type]="!status ? 'fullscreen' : 'fullscreen-exit'"></i>
  </div>
  `,
  host: {
    '[class.d-block]': 'true',
  },
})
export class HeaderFullScreenComponent extends AppComponentBase {
  status = false;

  constructor(
    injector: Injector
  ) {
    super(injector);
  }
  @HostListener('window:resize')
  _resize() {
    this.status = screenfull.isFullscreen;
  }

  @HostListener('click')
  _click() {
    if (screenfull.enabled) {
      screenfull.toggle();
    }
  }
}
