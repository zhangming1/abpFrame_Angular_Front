import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    Injector,
} from '@angular/core';
import {
    PagedListingComponentBase,
    PagedRequestDto,
} from '@shared/common/paged-listing-component-base';
import {
    DictionaryValueListDto,
    DictionaryValueServiceProxy,
    PagedResultDtoOfDictionaryValueListDto
} from '@shared/service-proxies/service-proxies';
import { NzTreeNode } from 'ng-zorro-antd';
import { finalize } from 'rxjs/operators';
import * as _ from 'lodash';
import { CreateOrEditDictionaryValueModalComponent } from './create-or-edit-dictionary-value-modal.component';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'dictionary-values',
    templateUrl: './dictionary-values.component.html',
    styleUrls: []
})
export class DictionaryValuesComponent extends PagedListingComponentBase<DictionaryValueListDto> implements OnInit {
    filterText = '';
    private _dictionaryItem: NzTreeNode = null;

    get dictionaryItem(): NzTreeNode {
        return this._dictionaryItem;
    }

    set dictionaryItem(ou: NzTreeNode) {
        if (this._dictionaryItem === ou) {
            return;
        }
        this._dictionaryItem = ou;
        if (ou) {
            this.refresh();
        }
    }

    constructor(
        injector: Injector,
        private _dictionaryValueService: DictionaryValueServiceProxy,
        private _activatedRoute: ActivatedRoute
    ) {
        super(injector);
        this.filterText = this._activatedRoute.snapshot.queryParams['filterText'] || '';
    }
    ngOnInit() { }

    protected fetchDataList(
        request: PagedRequestDto,
        pageNumber: number,
        finishedCallback: () => void,
    ): void {
        if (!this._dictionaryItem) {
            return;
        }

        this._dictionaryValueService
            .getPageData(
                this.filterText,
                parseInt(this._dictionaryItem.key, 10),
                request.sorting,
                request.maxResultCount,
                request.skipCount,
            )
            .pipe(finalize(finishedCallback))
            .subscribe((result: PagedResultDtoOfDictionaryValueListDto) => {
                this.dataList = result.items;
                this.showPaging(result);
            });
    }

    removeValue(dvalue: DictionaryValueListDto): void {
        const _ouId = parseInt(this.dictionaryItem.key);
        this._dictionaryValueService.delete(dvalue.id).subscribe(() => {
            this.refresh();
            this.notify.success(this.l('SuccessfullyRemoved'));
        });
    }

    batchDelete(): void {
        const selectCount = this.selectedDataItems.length;
        if (selectCount <= 0) {
            abp.message.warn(this.l('SelectAnItem'));
            return;
        }
        this.message.confirm(
            this.l('<b class="text-red">{0}</b> 条数据将被删除.', selectCount),
            this.l('AreYouSure'),
            res => {
                if (res) {
                    const ids = _.map(this.selectedDataItems, 'id');
                    this._dictionaryValueService
                        .batchDelete(ids)
                        .subscribe(() => {
                            this.refresh();
                            this.notify.success(this.l('SuccessfullyDeleted'));
                        });
                }
            },
        );
    }

    addValues(): void {
        this.modalHelper
            .static(CreateOrEditDictionaryValueModalComponent, {
                dictionaryValue: {
                    itemId: parseInt(this.dictionaryItem.key),
                    itemName: this.dictionaryItem.title
                }
            })
            .subscribe((result: DictionaryValueListDto) => {
                if (result) {
                    this.refresh();
                }
            });
    }
}
