import {
    ListResultDtoOfDictionaryItemListDto,
    DictionaryItemListDto
} from '@shared/service-proxies/service-proxies';
import {
    Component,
    OnInit,
    EventEmitter,
    Output,
    Injector,
    TemplateRef,
} from '@angular/core';
import {
    NzTreeNode,
    NzDropdownContextComponent,
    NzDropdownService,
    NzFormatEmitEvent,
    NzTreeBaseService,
} from 'ng-zorro-antd';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    DictionaryItemServiceProxy
} from '@shared/service-proxies/service-proxies';
import { ArrayService } from '@delon/util';
import * as _ from 'lodash';
import { finalize, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { CreateOrEditDictionaryItemModalComponent } from './create-or-edit-dictionary-item-modal.component';

@Component({
    selector: 'dictionary-item',
    templateUrl: './dictionary-item.component.html',
    styleUrls: ['./dictionary-item.less'],
})
export class DictionaryItemComponent extends AppComponentBase implements OnInit {

    @Output()
    selectedChange = new EventEmitter<NzTreeNode>();

    loading = false;
    totalItemCount = 0;
    _treeData: NzTreeNode[] = [];
    private _ouData: DictionaryItemListDto[] = [];
    contextMenu: NzDropdownContextComponent;
    activedNode: NzTreeNode;
    private dragSrcNode: NzTreeNode;
    private dragTargetNode: NzTreeNode;
    draging = false;

    constructor(
        injector: Injector,
        private _dictionaryItemService: DictionaryItemServiceProxy,
        private _nzDropdownService: NzDropdownService,
        private _arrayService: ArrayService,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.reload();
    }

    reload(): void {
        this.getTreeDataFromServer(treeData => {
            this._treeData = treeData;
        });
    }

    private getTreeDataFromServer(callback: (ous: NzTreeNode[]) => void): void {
        this.loading = true;
        this._dictionaryItemService
            .getAll()
            .pipe(
                finalize(() => {
                    this.loading = false;
                }),
            )
            .subscribe((result: ListResultDtoOfDictionaryItemListDto) => {
                this._ouData = result.items;
                this.totalItemCount = this._ouData.length;
                this._treeData = this._arrayService.arrToTreeNode(this._ouData, { parentIdMapName: 'parentId', titleMapName: 'name'});
            });
    }

    openFolder(data: NzTreeNode | NzFormatEmitEvent): void {
        if (data instanceof NzTreeNode) {
            if (!data.isExpanded) {
                data.origin.isLoading = true;
                setTimeout(() => {
                    data.isExpanded = !data.isExpanded;
                    data.origin.isLoading = false;
                }, 500);
            } else {
                data.isExpanded = !data.isExpanded;
            }
        } else {
            if (!data.node.isExpanded) {
                data.node.origin.isLoading = true;
                setTimeout(() => {
                    data.node.isExpanded = !data.node.isExpanded;
                    data.node.origin.isLoading = false;
                }, 500);
            } else {
                data.node.isExpanded = !data.node.isExpanded;
            }
        }
    }

    activeNode(data: NzFormatEmitEvent): void {
        this._setActiveNodeValue(data.node);
    }

    private _setActiveNodeValue(currentNode: NzTreeNode) {
        this._setActiveNodeNull(false);
        currentNode.isSelected = true;
        this.activedNode = currentNode;
        this.selectedChange.emit(currentNode);
    }

    private _setActiveNodeNull(isEmit: boolean = true) {
        if (this.activedNode) {
            this.activedNode = null;
            if (isEmit) {
                this.selectedChange.emit(null);
            }
        }
    }

    dragEnter(event: NzFormatEmitEvent): void {
        this.dragSrcNode = null;
        this.dragTargetNode = null;
        this.dragSrcNode = event.dragNode;
        this.dragTargetNode = event.node;
    }

    dragSaveData(event: NzFormatEmitEvent): void {
        // if (this.dragSrcNode && this.dragTargetNode) {
        //     if (this.dragSrcNode.key !== this.dragTargetNode.key) {
        //         this.draging = true;
        //         this.message.confirm(
        //             this.l(
        //                 'OrganizationItemMoveConfirmMessage',
        //                 this.dragSrcNode.title,
        //                 this.dragTargetNode.title,
        //             ),
        //             this.l('AreYouSure'),
        //             isConfirmed => {
        //                 if (isConfirmed) {
        //                     const input = new MoveOrganizationItemInput();
        //                     // tslint:disable-next-line:radix
        //                     input.id = parseInt(this.dragSrcNode.key);
        //                     input.newParentId =
        //                         this.dragTargetNode === null
        //                             ? undefined
        //                             : // tslint:disable-next-line:radix
        //                             parseInt(this.dragTargetNode.key);
        //                     this._organizationItemService
        //                         .moveOrganizationItem(input)
        //                         .pipe(
        //                             finalize(() => {
        //                                 this.draging = false;
        //                                 this.reload();
        //                             }),
        //                             catchError(error => {
        //                                 return throwError(error);
        //                             }),
        //                         )
        //                         .subscribe(() => {
        //                             this.notify.success(this.l('SuccessfullyMoved'));
        //                         });
        //                 } else {
        //                     this.reload();
        //                     this.draging = false;
        //                 }
        //             },
        //         );
        //     }
        // }
    }

    createContextMenu(
        $event: MouseEvent,
        template: TemplateRef<void>,
        node: NzTreeNode,
    ): void {
        this.contextMenu = this._nzDropdownService.create($event, template);
        this._setActiveNodeValue(node);
    }

    addItem(parentId?: number): void {
        if (!parentId) {
            this._setActiveNodeNull();
        }
        let _parentName = null;
        if (this.activedNode) {
            _parentName = this.activedNode.title;
        }
        this.modalHelper
            .createStatic(CreateOrEditDictionaryItemModalComponent, {
                dictionaryItem: {
                    parentId: parentId,
                    parentName: _parentName
                },
            }, { size: 'md' })
            .subscribe((res: DictionaryItemListDto) => {
                if (res) {
                    this.itemCreated(res);
                }
            });
    }

    itemCreated(ou: DictionaryItemListDto): void {
        this.reload();
    }

    addSubItem() {
        const canManageOrganizationTree = this.isGranted(
            'Pages.DictionaryItem.Create',
        );
        if (!canManageOrganizationTree) {
            return;
        }
        if (this.activedNode.key) {
            this.addItem(parseInt(this.activedNode.key));
        }
        this.contextMenu.close();
    }

    editItem(): void {
        const canEdit = this.isGranted(
            'Pages.DictionaryItem.Edit',
        );
        if (!canEdit) {
            return;
        }
        if (this.activedNode.key) {
            const ouPars = {
                id: parseInt(this.activedNode.key),
                name: this.activedNode.title,
                code: this.activedNode.origin.code,
                sortCode: this.activedNode.origin.sortCode
            };
            console.log(this.activedNode);
            this.modalHelper
                .createStatic(CreateOrEditDictionaryItemModalComponent, {
                    dictionaryItem: ouPars
                }, { size: 'md' })
                .subscribe((res: DictionaryItemListDto) => {
                    if (res) {
                        this.activedNode.title = res.name;
                    }
                });
        }
        this.contextMenu.close();
    }

    deleteItem(): void {
        if (this.activedNode.key) {
            this.message.confirm(
                this.l('CommonDeleteWarningMessage', this.activedNode.title),
                this.l('AreYouSure'),
                isConfirmed => {
                    if (isConfirmed) {
                        this._dictionaryItemService
                            .delete(parseInt(this.activedNode.key, 10))
                            .subscribe(() => {
                                this.totalItemCount -= 1;
                                this.itemDeletedData();
                                this.notify.success(this.l('SuccessfullyDeleted'));
                            });
                    }
                });
        }
        this.contextMenu.close();
    }

    private itemDeletedData(): void {
        this.reload();
    }

    private _itemDeletedSubData(item: NzTreeNode): void {
        if (item && item.children) {
            item.children.forEach(itemChild => {
                if (itemChild.key === this.activedNode.key) {
                    _.remove(item.children, remove => {
                        return remove.key === this.activedNode.key;
                    });
                    item.isLeaf = !item.children.length;
                    this._setActiveNodeNull();
                    return;
                }
                this._itemDeletedSubData(itemChild);
            });
        }
    }

    incrementValueCount(incrementAmount: number): void {
        this.activedNode.origin.valueCount =
            this.activedNode.origin.valueCount + incrementAmount;
        if (this.activedNode.origin.valueCount < 0) {
            this.activedNode.origin.valueCount = 0;
        }
    }
}
